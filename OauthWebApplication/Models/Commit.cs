﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OauthWebApplication.Models
{
    public class Commit
    {
        
        public int Id { get; set; }
        public string Sha { get; set; }
        public string CommiterName { get; set; }
        public string CommiterEmail { get; set; }
        public string Message { get; set; }        
        public DateTime CommitDate { get; set; }

        public int? RepoId { get; set; }
        public Repo Repo { get; set; }    
    }
}