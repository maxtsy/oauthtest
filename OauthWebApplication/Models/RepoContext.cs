﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace OauthWebApplication.Models
{
    public class RepoContext : DbContext
    {
        public DbSet<Commit> Commits { get; set; }
        public DbSet<Repo> Repos { get; set; }
    }
}