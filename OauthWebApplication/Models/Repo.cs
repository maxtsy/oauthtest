﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OauthWebApplication.Models
{
    public class Repo
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Reponame { get; set; }
        public string Branch { get; set; }

        public ICollection<Commit> Commits { get; set; }

        public Repo()
        {
            Commits = new List<Commit>();
        }
    }
}