﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Octokit;
using OauthWebApplication.Models;

namespace OauthWebApplication.Controllers
{
    public class HomeController : Controller
    {
        const string clientId = "7719ddbc076023a1076c";

        private const string clientSecret = "3ef70af57a1a8ef70fe9ab5a6fd89e9ba484b2c0";

        readonly GitHubClient client =
            new GitHubClient(new ProductHeaderValue("OAuthTestClient"), new Uri("https://github.com/"));

        // Структура управления
        public enum Command
        {
            Empty = 0,
            SaveAndView = 1,
            View = 2,
            Update = 3                      
        }

        // Контекст
        RepoContext db = new RepoContext();

        // Главная        
        public ActionResult Index()
        {
            var accessToken = Session["OAuthToken"] as string;
            if (accessToken != null)
            {
                client.Credentials = new Credentials(accessToken);
            }
            ViewBag.AccessToken = accessToken;

            return View();
        }

        // Пробуем авторизовать
        public async Task<ActionResult> Authorization()
        {
            try
            {
                // в случае отсутствия токина, поднимется исключение и кидает на метод авторизации через GitHub  
                var repositories = await client.Repository.GetAllForCurrent();

                return RedirectToAction("Index");
            }
            catch (AuthorizationException)
            {
                return Redirect(GetOauthLoginUrl());
            }
        }

        // Страница просмотра коммитов
        public async Task<ActionResult> Commits(string owner, string repo, Command command = Command.Empty)
        {
            var accessToken = Session["OAuthToken"] as string;
            client.Credentials = new Credentials(accessToken);

            if (string.IsNullOrEmpty(owner) || string.IsNullOrEmpty(owner))
                return RedirectToAction("Error", "Home", new { owner = owner, repo = repo });

            int existingRepo = db.Repos.Where(r => r.Username == owner && r.Reponame == repo).Count<Repo>();
            if (existingRepo == 0)
                return RedirectToAction("Error", "Home", new { owner = owner, repo = repo });

            IList<Models.Commit> commitsList = new List<Models.Commit>();
            var repolocal = db.Repos.Where(r => r.Username == owner && r.Reponame == repo).First<Models.Repo>();

            // Сохраняем и показываем таблицу с коммитами
            if (Command.SaveAndView == command)
            {                
                var commits = await client.Repository.Commit.GetAll(owner, repo);

                SaveCommits(repolocal.Id, commits);

                commitsList = db.Commits.Where(c => c.RepoId == repolocal.Id).OrderByDescending(c => c.CommitDate).ToList<Models.Commit>();
            }
            // Просмотр таблицы с коммитами
            else if (Command.View == command)
            {
                commitsList = db.Commits.Where(c => c.RepoId == repolocal.Id).OrderByDescending(c => c.CommitDate).ToList<Models.Commit>();
            }
            // Обновление коммитов
            else if (Command.Update == command)
            {
                var commitsToDelete = db.Commits.Where(c => c.RepoId == repolocal.Id).ToList<Models.Commit>();

                db.Commits.RemoveRange(commitsToDelete);
                db.SaveChanges();               

                var commits = await client.Repository.Commit.GetAll(owner, repo);

                SaveCommits(repolocal.Id, commits);

                commitsList = db.Commits.Where(c => c.RepoId == repolocal.Id).OrderByDescending(c => c.CommitDate).ToList<Models.Commit>();
            }
            // И вдруг если ничего не вышло
            else if (Command.Empty == command)
                return RedirectToAction("Index");

            ViewBag.owner = owner;
            ViewBag.repo = repo;

            return View(commitsList);
        }

        // Страница ошибки, вдруг если введены неверные данные
        public ActionResult Error(string owner, string repo)
        {
            ViewBag.owner = owner;
            ViewBag.repo = repo;

            return View();
        }       

        // Обработчик формы запроса коммитов        
        [HttpPost]
        public async Task<RedirectToRouteResult> Repos(Repo repo)
        {
            int existingRepo = db.Repos.Where(r => r.Username == repo.Username).Count<Repo>();
            // Если владелец и реп существуют, возвращаем коммиты из БД
            if (existingRepo > 0)
            {
                return RedirectToAction("Commits", "Home", new { owner = repo.Username, repo = repo.Reponame, command = Command.View });
            }
            else
            {
                // 
                var accessToken = Session["OAuthToken"] as string;
                if (accessToken != null)
                {
                    client.Credentials = new Credentials(accessToken);
                }

                // Пробуем получить пробные данные из репозитория
                // в случае если ок, то сохраняем владельца и реп и перекидываем на страницу просмотра коммитов
                // в случае исключения, кидаем на страницу ошибки
                try
                {
                    var existingName = await client.Repository.Get(repo.Username, repo.Reponame);

                    db.Repos.Add(repo);
                    db.SaveChanges();

                    return RedirectToAction("Commits", "Home", new { owner = repo.Username, repo = repo.Reponame, command = Command.SaveAndView });
                }
                catch (NotFoundException)
                {
                    return RedirectToAction("Error", "Home", new { owner = repo.Username, repo = repo.Reponame });
                }
            }
        }

        // Получение токина GitHub        
        public async Task<ActionResult> Authorize(string code, string state)
        {
            if (!String.IsNullOrEmpty(code))
            {
                var expectedState = Session["CSRF:State"] as string;
                if (state != expectedState) throw new InvalidOperationException("С безопасностью проблемы!");
                Session["CSRF:State"] = null;

                var token = await client.Oauth.CreateAccessToken(
                    new OauthTokenRequest(clientId, clientSecret, code)
                    {
                        RedirectUri = new Uri("http://localhost:49752/home/authorize")
                    });
                Session["OAuthToken"] = token.AccessToken;
            }

            return RedirectToAction("Index");
        }

        // Запрос на GitHub        
        private string GetOauthLoginUrl()
        {
            string csrf = Membership.GeneratePassword(24, 1);
            Session["CSRF:State"] = csrf;
            
            var request = new OauthLoginRequest(clientId)
            {
                Scopes = { "user", "notifications", "repositories" },
                State = csrf
            };
            var oauthLoginUrl = client.Oauth.GetGitHubLoginUrl(request);
            return oauthLoginUrl.ToString();
        }        
        
        // Сохранение коммитов          
        private void SaveCommits(int repoId, IReadOnlyList<GitHubCommit> commits)
        {
            string message;

            IList<Models.Commit> lcommits = new List<Models.Commit>();

            foreach (var c in commits)
            {
                if (c.Commit.Message.Length > 130)
                {
                    message = c.Commit.Message.Substring(0, 130);
                    message = string.Concat(message, "...");
                }                    
                else
                    message = c.Commit.Message;

                lcommits.Add(new Models.Commit() { Sha = c.Sha, CommiterName = c.Commit.Committer.Name, CommiterEmail = c.Commit.Committer.Email, Message = message, CommitDate = c.Commit.Committer.Date.LocalDateTime, RepoId = repoId });
            }

            db.Commits.AddRange(lcommits);
            db.SaveChanges();
        }                   
       
        // Удаление коммитов через AJAX
        public string PurgeCommits(string ids)
        {
            string[] commitsIds = ids.Split(',');            

            if (commitsIds.Length > 0)
            {
                IList<Models.Commit> commitsToDelete = new List<Models.Commit>();

                foreach (string id in commitsIds)
                {
                    int cId = Int32.Parse(id);

                    var commit = db.Commits.Where(c => c.Id == cId).First<Models.Commit>();
                    commitsToDelete.Add(commit);
                }

                db.Commits.RemoveRange(commitsToDelete);
                db.SaveChanges();

                return "ok";
            } else
            {
                return "error";
            }                        
        }            
    }
}