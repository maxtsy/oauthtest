﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Owin.Security.Providers.GitHub;
using System.Security.Claims;

[assembly: OwinStartup(typeof(OauthWebApplication.Startup))]

namespace OauthWebApplication
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Дополнительные сведения о настройке приложения см. по адресу: http://go.microsoft.com/fwlink/?LinkID=316888

            // Хотелось сделать через OWIN, но ...

            app.SetDefaultSignInAsAuthenticationType("ExternalCookie");

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ExternalCockie",
                AuthenticationMode = AuthenticationMode.Passive,
                CookieName = ".AspNet.ExternaCookie",
                ExpireTimeSpan = TimeSpan.FromMinutes(5),    
            });

            app.UseGitHubAuthentication(new GitHubAuthenticationOptions
            {
                ClientId = "7719ddbc076023a1076c",
                ClientSecret = "3ef70af57a1a8ef70fe9ab5a6fd89e9ba484b2c0",
                Provider = new GitHubAuthenticationProvider
                {
                    OnAuthenticated = context =>
                    {
                        context.Identity.AddClaim(new Claim("urn:token:github", context.AccessToken));

                        return Task.FromResult(true);
                    }
                }
            });
        }
    }
}
