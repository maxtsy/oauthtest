﻿$(document).ready(function () {
    var purgeBtn = $('#purge'),
        updateBtn = $('#update');
    var table = $('#commits').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json"
        },        
        "lengthMenu": [[20], [20]],
        "bFilter": false,
        "ordering": false,
        "order": [[ 5, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false,

            }
        ]
    });
    
    /* Выделение строк */
    $('#commits tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');

        if (table.rows('.selected').data().length > 0)
            purgeBtn.removeAttr('disabled');
        else
            purgeBtn.attr('disabled', 'true');
    });
    /* Удаление */
   purgeBtn.click(function () {
       if (table.rows('.selected').data().length > 0) {
           
           var data = table.rows('.selected').data(),
               l = table.rows('.selected').data().length,
               ids = [];
           for (var i = 0; i < l; i++)
               ids.push(data[i][0]);         

           ids.join();

           if (ids)
               $.ajax({
                   method: "GET",
                   url: "/Home/PurgeCommits?ids=" + ids
               }).
                done(function (msg) {
                    console.log(msg);
                    if (msg == 'ok')                        
                        table.rows('.selected').remove().draw(false);
                }).
                fail(function (jqXHR, textStatus) {
                   alert("Проблема с запросом: " + textStatus);
                });           
       }
   });       
});